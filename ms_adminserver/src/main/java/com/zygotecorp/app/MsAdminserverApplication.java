package com.zygotecorp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsAdminserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsAdminserverApplication.class, args);
	}

}
