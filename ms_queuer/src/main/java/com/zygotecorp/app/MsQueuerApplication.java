package com.zygotecorp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsQueuerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsQueuerApplication.class, args);
	}

}
